module gitlab.com/lomemory/rangelist

go 1.17

require (
	github.com/go-playground/assert/v2 v2.0.1
	github.com/magiconair/properties v1.8.5
)
