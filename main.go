package main

import (
	"fmt"
	"strings"
)

// Task: Implement a struct named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.
type RangeList struct {
	list [][2]int
}

// merge two range (targetElement,rangeElement)
func (rangeList *RangeList) mergeRange(targetElement, rangeElement [2]int) [2]int {
	mergeElement := [2]int{targetElement[0], targetElement[1]}
	if targetElement[0] > rangeElement[0] {
		targetElement[0] = rangeElement[0]
		mergeElement[0] = rangeElement[0]
	}
	if targetElement[1] < rangeElement[1] {
		mergeElement[1] = rangeElement[1]
	}
	return mergeElement
}

// check if rangeElement have overlap with targetElement
func (rangeLis *RangeList) haveOverlap(targetElement, rangeElement [2]int) bool {
	return !(targetElement[1] <= rangeElement[0] || rangeElement[1] <= rangeElement[0])
}

// remove rangeElement from targetElement
func (rangeList *RangeList) remove(targetElement, rangeElement [2]int) (ret [][2]int) {

	ret = make([][2]int, 0)
	a := [][2]int{{targetElement[0], rangeElement[0]}, {}, {rangeElement[1], targetElement[1]}}
	for _, v := range a {
		if v[1] > v[0] {
			ret = append(ret, v)
		}
	}
	return
}

// add rangeElement to rangeList
func (rangeList *RangeList) Add(rangeElement [2]int) (err error) {
	if rangeElement[0] < rangeElement[1] {
		for i := 0; i < len(rangeList.list); i++ {
			start, end := rangeList.list[i][0], rangeList.list[i][1]
			if rangeElement[1] < start {
				rangeList.list = append(rangeList.list[:i], append([][2]int{rangeElement}, rangeList.list[i:]...)...)
				return
			}
			if rangeElement[0] <= end {
				mergeElement := rangeList.mergeRange(rangeElement, rangeList.list[i])
				rangeList.list = append(rangeList.list[:i], rangeList.list[i+1:]...)
				rangeList.Add(mergeElement)
				return
			}

		}
		rangeList.list = append(rangeList.list, rangeElement)
	}

	return
}

// remove rangeElement from rangeList
func (rangeList *RangeList) Remove(rangeElement [2]int) (err error) {
	if rangeElement[0] > rangeElement[1] {
		return
	}
	for i := 0; i < len(rangeList.list); i++ {
		start := rangeList.list[i][0]
		if rangeElement[1] < start {
			return
		}
		// if  rangeList item have overlap with rangeElement, remove it's overlap part
		if rangeList.haveOverlap(rangeList.list[i], rangeElement) {
			ret := rangeList.remove(rangeList.list[i], rangeElement)
			rangeList.list = append(rangeList.list[:i], append(ret, rangeList.list[i+1:]...)...)
			i += (len(ret) - 1)
		}
	}
	return
}

// Print
func (rangeList *RangeList) Print() (err error) {
	stringList := make([]string, len(rangeList.list))
	for i, v := range rangeList.list {
		stringList[i] = fmt.Sprintf("[%d, %d)", v[0], v[1])
	}
	fmt.Println(strings.Join(stringList, " "))
	return
}

func main() {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print() // Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print() // Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print() // Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print() // Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print() // Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print() // Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print() // Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print() // Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print() // Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print() // Should display: [1, 3) [19, 21)
}
