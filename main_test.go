package main

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func TestRangeList_Add(t *testing.T) {
	type fields struct {
		list [][2]int
	}
	type args struct {
		rangeElement [2]int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		result [][2]int
	}{
		{
			name: "case 1",
			fields: fields{
				list: [][2]int{},
			},
			args: args{
				rangeElement: [2]int{1, 2},
			},
			result: [][2]int{{1, 2}},
		},
		{
			name: "case 2",
			fields: fields{
				list: [][2]int{{1, 5}},
			},
			args: args{
				rangeElement: [2]int{1, 2},
			},
			result: [][2]int{{1, 5}},
		},
		{
			name: "case 3",
			fields: fields{
				list: [][2]int{{1, 5}},
			},
			args: args{
				rangeElement: [2]int{5, 6},
			},
			result: [][2]int{{1, 6}},
		},
		{
			name: "case 4",
			fields: fields{
				list: [][2]int{{1, 5}},
			},
			args: args{
				rangeElement: [2]int{6, 7},
			},
			result: [][2]int{{1, 5}, {6, 7}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rangeList := &RangeList{
				list: tt.fields.list,
			}
			if err := rangeList.Add(tt.args.rangeElement); err != nil {
				t.Errorf("RangeList.Add() error = %v", err)
			}
			assert.Equal(t, rangeList.list, tt.result)
		})
	}
}

func TestRangeList_Remove(t *testing.T) {
	type fields struct {
		list [][2]int
	}
	type args struct {
		rangeElement [2]int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		result [][2]int
	}{
		{
			name: "case 1",
			fields: fields{
				list: [][2]int{},
			},
			args: args{
				rangeElement: [2]int{1, 2},
			},
			result: [][2]int{},
		},
		{
			name: "case 2",
			fields: fields{
				list: [][2]int{{5, 10}},
			},
			args: args{
				rangeElement: [2]int{1, 7},
			},
			result: [][2]int{{7, 10}},
		},
		{
			name: "case 3",
			fields: fields{
				list: [][2]int{{5, 10}},
			},
			args: args{
				rangeElement: [2]int{7, 12},
			},
			result: [][2]int{{5, 7}},
		},
		{
			name: "case 4",
			fields: fields{
				list: [][2]int{{5, 10}},
			},
			args: args{
				rangeElement: [2]int{6, 7},
			},
			result: [][2]int{{5, 6}, {7, 10}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rangeList := &RangeList{
				list: tt.fields.list,
			}
			if err := rangeList.Remove(tt.args.rangeElement); err != nil {
				t.Errorf("RangeList.Add() error = %v", err)
			}
			assert.Equal(t, rangeList.list, tt.result)
		})
	}
}
