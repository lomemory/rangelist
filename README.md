# rangelist
> // Task: Implement a struct named 'RangeList'

> // A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.

> // A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)

> // NOTE: Feel free to add any extra member variables/functions you like.

## Table of Contents

- [rangelist](#rangelist)
  - [Table of Contents](#table-of-contents)
  - [Requirements](#requirements)
  - [Usage](#usage)

## Requirements

requires the following to run:

- go ^1.17

## Usage

```
    go run main.go
```
